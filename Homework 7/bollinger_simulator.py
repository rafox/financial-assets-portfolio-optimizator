# -*- coding: utf-8 -*-
"""
Created on Sat Apr 27 21:28:09 2013

@author: Rafox
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Apr 27 12:50:00 2013

@author: Rafox
"""

import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import csv
import datetime
from datetime import date, timedelta
import matplotlib.pyplot as plt
import sys
from pylab import *
from copy import deepcopy

def getData(startdate, enddate, symbols):
    dt_start = startdate
    dt_end = enddate
    dt_timeofday = dt.timedelta(hours=16)
    
    
    
    # Get the working NYSE days   
    c_dataobj = da.DataAccess('Yahoo') #Connect to Yahoo to get prices
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday) #Get the working days of the NYSE
    ls_all_syms = c_dataobj.get_all_symbols()#Get Data for all the symbols of NYSE
    ls_symbols = c_dataobj.get_symbols_from_list('sp5002012')
    ls_symbols.append('SPY')          #Get the symbols in interest
    ls_bad_syms = list(set(ls_symbols) - set(ls_all_syms)) #Remove non-existent (bad) symbols
    
    if len(ls_bad_syms) != 0: #If there is any wrong typed symbol in the ls_symbols
        print "Portfolio contains bad symbols : ", ls_bad_syms

        for s_sym in ls_bad_syms: #Remove the bad symbols
            i_index = ls_symbols.index(s_sym)
            ls_symbols.pop(i_index)
            ls_symbols.pop(i_index)
    
    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close'] #Create the keys we want to return

    ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys) #Get the Stock Data
    #print ldf_data
    d_data = dict(zip(ls_keys, ldf_data)) #Create a dictionary to make easy to access the data
    #print d_data
    
    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method = 'ffill')
        d_data[s_key] = d_data[s_key].fillna(method = 'bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)
        
    return d_data
    
def bollingerBand(data, period):
    means = pd.rolling_mean(data, period, min_periods=period)
    std = pd.rolling_std(data, period, min_periods=period)
    
    upper = means+std
    lower = means-std
    
    return means, upper, lower, std
    
def find_events_bollinger(ls_symbols, d_data):
    ''' Finding the event dataframe '''
    df_actualClose = d_data['close']
    ts_market = df_actualClose['SPY']
    market = 'SPY'
    
    print "Finding Events"

    # Creating an empty dataframe
    df_events = deepcopy(df_actualClose)
    df_events = df_events * np.NAN
    df_eventsSell = deepcopy(df_events)

    # Time stamps for the event range
    ldt_timestamps = df_actualClose.index
    total = 0
    sell = 0
    
    #Get and normalize bollinger bands
    means, upper, lower, std = bollingerBand(df_actualClose, 20)
    bollinger = (df_actualClose - means)/std
    
    

    for s_sym in ls_symbols:
        for i in range(1, len(ldt_timestamps)-1):

            
            # Calculating the returns for this timestamp
            f_symprice_today = df_actualClose[s_sym].ix[ldt_timestamps[i]]
            f_symprice_yest = df_actualClose[s_sym].ix[ldt_timestamps[i - 1]]
            f_marketprice_today = ts_market.ix[ldt_timestamps[i]]
            f_marketprice_yest = ts_market.ix[ldt_timestamps[i - 1]]
            f_symreturn_today = (f_symprice_today / f_symprice_yest) - 1
            f_marketreturn_today = (f_marketprice_today / f_marketprice_yest) - 1
            
            #Calculate Bollinger Parameters
            f_symboll_today = bollinger[s_sym].ix[ldt_timestamps[i]]
            f_symboll_yest = bollinger[s_sym].ix[ldt_timestamps[i-1]]
            f_marketboll_today = bollinger[market].ix[ldt_timestamps[i]]
            f_marketboll_yest = bollinger[market].ix[ldt_timestamps[i-1]]

            # Event is found if the symbol is down more then 3% while the
            # market is up more then 2%
            if s_sym != 'SPY' and evento_boll(f_symboll_today, f_symboll_yest, f_marketboll_today, f_marketboll_yest):
                df_events[s_sym].ix[ldt_timestamps[i]] = 1 #(COMPRA)
                total = total +1
                if i+5 > 500:
                    print str(i+5) + " - " + str((len(ldt_timestamps)-1))
                minimo = min([len(ldt_timestamps)-1, i+5])
                df_eventsSell[s_sym].ix[ldt_timestamps[minimo]] = 1 #(VENDE)
                sell = sell + 1
        
         
    print total  
    print sell
    return df_events, df_eventsSell
    
def evento_boll(f_symboll_today, f_symboll_yest, f_marketboll_today, f_marketboll_yest):
    if f_symboll_today < -2.0 and f_symboll_yest >= -2.0 and f_marketboll_today >= 1.2:
        return 1
    return 0
            
def writeOrdersCSV(arquivo, df_events, df_eventsSell, daysToHold):
    sell = 0
    buy = 0
    with open(arquivo, "wb") as ifile:
        writer = csv.writer(ifile)
        
        for indexData, row in df_events.iterrows():
            for indexStock in row.index:
               # print row.ix[indexStock]
                if row.ix[indexStock] == 1:
                    buy=buy+1
                    stock = indexStock
                    quant = 100
                
                    ano = indexData.year
                    mes = str(indexData.month)
                    dia = str(indexData.day)
                    teste = writer.writerow([ano, mes, dia, stock, "Buy", quant])

        for indexData, row in df_eventsSell.iterrows():
            for indexStock in row.index:            
                if row.ix[indexStock] == 1:
                    sell = sell+1
                    stock = indexStock
                    quant = 100
                
                    ano = indexData.year
                    mes = str(indexData.month)
                    dia = str(indexData.day)
                    teste = writer.writerow([ano, mes, dia, stock, "Sell", quant])
         
    print buy
    print sell
    return
            
print "Bollinger"

dt_start = dt.datetime(2008, 1, 1)
dt_end = dt.datetime(2009, 12, 31)
ls_data = getData(dt_start, dt_end, 0)


c_dataobj = da.DataAccess('Yahoo') #Connect to Yahoo to get prices
symbols = c_dataobj.get_symbols_from_list('sp5002012')
df_events, df_eventsSell = find_events_bollinger(symbols, ls_data)
print "Creating Orders"
writeOrdersCSV("saidaOrdens_boll.csv", df_events, df_eventsSell, 0)
print ls_data
ep.eventprofiler(df_events, ls_data, i_lookback=20, i_lookforward=20, s_filename='MyEventStudy-66.pdf', b_market_neutral=True, b_errorbars=True, s_market_sym='SPY')