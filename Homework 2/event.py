'''
(c) 2011, 2012 Georgia Tech Research Corporation
This source code is released under the New BSD license.  Please see
http://wiki.quantsoftware.org/index.php?title=QSTK_License
for license details.

Created on January, 23, 2013

@author: Sourabh Bajaj
@contact: sourabhbajaj@gatech.edu
@summary: Event Profiler Tutorial
'''


import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import csv

"""
Accepts a list of symbols along with start and end date
Returns the Event Matrix which is a pandas Datamatrix
Event matrix has the following structure :
    |IBM |GOOG|XOM |MSFT| GS | JP |
(d1)|nan |nan | 1  |nan |nan | 1  |
(d2)|nan | 1  |nan |nan |nan |nan |
(d3)| 1  |nan | 1  |nan | 1  |nan |
(d4)|nan |  1 |nan | 1  |nan |nan |
...................................
...................................
Also, d1 = start date
nan = no information about any event.
1 = status bit(positively confirms the event occurence)
"""


def find_events(ls_symbols, d_data):
    ''' Finding the event dataframe '''
    df_actualClose = d_data['actual_close']
    ts_market = df_actualClose['SPY']

    print "Finding Events"

    # Creating an empty dataframe
    df_events = copy.deepcopy(df_actualClose)
    df_events = df_events * np.NAN
    df_eventsSell = copy.deepcopy(df_events)

    # Time stamps for the event range
    ldt_timestamps = df_actualClose.index
    total = 0
    sell = 0

    for s_sym in ls_symbols:
        for i in range(1, len(ldt_timestamps)-1):

            
            # Calculating the returns for this timestamp
            f_symprice_today = df_actualClose[s_sym].ix[ldt_timestamps[i]]
            f_symprice_yest = df_actualClose[s_sym].ix[ldt_timestamps[i - 1]]
            f_marketprice_today = ts_market.ix[ldt_timestamps[i]]
            f_marketprice_yest = ts_market.ix[ldt_timestamps[i - 1]]
            f_symreturn_today = (f_symprice_today / f_symprice_yest) - 1
            f_marketreturn_today = (f_marketprice_today / f_marketprice_yest) - 1

            # Event is found if the symbol is down more then 3% while the
            # market is up more then 2%
            if s_sym != 'SPY' and evento(f_symprice_today, f_symprice_yest, f_marketprice_today, f_marketprice_yest, f_symreturn_today, f_marketreturn_today):
                df_events[s_sym].ix[ldt_timestamps[i]] = 1 #(COMPRA)
                total = total +1
                if i+5 > 500:
                    print str(i+5) + " - " + str((len(ldt_timestamps)-1))
                minimo = min([len(ldt_timestamps)-1, i+5])
                df_eventsSell[s_sym].ix[ldt_timestamps[minimo]] = 1 #(VENDE)
                sell = sell + 1
        
         
    print total  
    print sell
    return df_events, df_eventsSell
        
def evento(f_symprice_today, f_symprice_yest, f_marketprice_today, f_marketprice_yest, f_symreturn_today, f_marketreturn_today):
    if f_symprice_yest >= 7.00 and f_symprice_today < 7.00:
        return 1
    else:
        return 0
    

def writeOrdersCSV(arquivo, df_events, df_eventsSell, daysToHold):
    sell = 0
    buy = 0
    with open(arquivo, "wb") as ifile:
        writer = csv.writer(ifile)
        
        for indexData, row in df_events.iterrows():
            for indexStock in row.index:
               # print row.ix[indexStock]
                if row.ix[indexStock] == 1:
                    buy=buy+1
                    stock = indexStock
                    quant = 100
                
                    ano = indexData.year
                    mes = str(indexData.month)
                    dia = str(indexData.day)
                    teste = writer.writerow([ano, mes, dia, stock, "Buy", quant])

        for indexData, row in df_eventsSell.iterrows():
            for indexStock in row.index:            
                if row.ix[indexStock] == 1:
                    sell = sell+1
                    stock = indexStock
                    quant = 100
                
                    ano = indexData.year
                    mes = str(indexData.month)
                    dia = str(indexData.day)
                    teste = writer.writerow([ano, mes, dia, stock, "Sell", quant])
         
    print buy
    print sell
    return
            

if __name__ == '__main__':
    dt_start = dt.datetime(2008, 1, 1)
    dt_end = dt.datetime(2009, 12, 31)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt.timedelta(hours=16))

    dataobj = da.DataAccess('Yahoo')
    ls_symbols = dataobj.get_symbols_from_list('sp5002012')
    ls_symbols.append('SPY')
    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ldf_data = dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))
    
    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method = 'ffill')
        d_data[s_key] = d_data[s_key].fillna(method = 'bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)

    df_events, df_eventsSell = find_events(ls_symbols, d_data)
    print "Creating Study"
    print d_data
    writeOrdersCSV("saidaOrdens.csv", df_events, df_eventsSell, 0)
    ep.eventprofiler(df_events, d_data, i_lookback=20, i_lookforward=20, s_filename='MyEventStudy-66.pdf', b_market_neutral=True, b_errorbars=True, s_market_sym='SPY')
    