# -*- coding: utf-8 -*-
"""
Created on Sat Apr 20 17:00:26 2013

@author: Rafox
"""

import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import csv
import datetime
from datetime import date, timedelta
import matplotlib.pyplot as plt
import sys
from pylab import *

def getData(startdate, enddate, symbols):
    dt_start = startdate
    dt_end = enddate
    dt_timeofday = dt.timedelta(hours=16)
    
    
    
    # Get the working NYSE days   
    c_dataobj = da.DataAccess('Yahoo', cachestalltime=0) #Connect to Yahoo to get prices
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday) #Get the working days of the NYSE
    ls_all_syms = c_dataobj.get_all_symbols()#Get Data for all the symbols of NYSE
    ls_symbols = symbols #Get the symbols in interest
    ls_bad_syms = list(set(ls_symbols) - set(ls_all_syms)) #Remove non-existent (bad) symbols
    
    if len(ls_bad_syms) != 0: #If there is any wrong typed symbol in the ls_symbols
        print "Portfolio contains bad symbols : ", ls_bad_syms

        for s_sym in ls_bad_syms: #Remove the bad symbols
            i_index = ls_symbols.index(s_sym)
            ls_symbols.pop(i_index)
            ls_symbols.pop(i_index)
    
    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close'] #Create the keys we want to return

    ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys) #Get the Stock Data
    #print ldf_data
    d_data = dict(zip(ls_keys, ldf_data)) #Create a dictionary to make easy to access the data
    #print d_data
    
    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method = 'ffill')
        d_data[s_key] = d_data[s_key].fillna(method = 'bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)
        
    return d_data
    
def bollingerBand(data, period):
    means = pd.rolling_mean(data, period, min_periods=period)
    std = pd.rolling_std(data, period, min_periods=period)
    
    upper = means+std
    lower = means-std
    
    return means, upper, lower, std
    
print "Bollinger"
symbols = ['AAPL', 'GOOG', 'IBM', 'MSFT']
dt_start = dt.datetime(2010, 1, 1)
dt_end = dt.datetime(2010, 12, 31)
period = 20

ls_data = getData(dt_start, dt_end, symbols)
adjclose = ls_data["close"]
means, upper, lower, std = bollingerBand(adjclose, period)

# Plot the prices
plt.clf()

symtoplot = 'AAPL'
plot(adjclose.index,adjclose[symtoplot].values,label=symtoplot)
plot(adjclose.index,means[symtoplot].values)
plot(adjclose.index, upper[symtoplot].values)
plot(adjclose.index, lower[symtoplot].values)
plt.legend([symtoplot,'Moving Avg.', 'Bollinger Upper', 'Bollinger Lower'])
plt.ylabel('Adjusted Close')
savefig("movingavg-ex.png", format='png')


bollinger = (adjclose - means)/std

plt.clf()
plot(adjclose.index, bollinger[symtoplot].values)
plt.legend(['Bollinger'])
plt.ylabel('Bollinger Value')
savefig("bollinger.png", format='png')