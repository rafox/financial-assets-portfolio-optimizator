'''
(c) 2011, 2012 Georgia Tech Research Corporation
This source code is released under the New BSD license.  Please see
http://wiki.quantsoftware.org/index.php?title=QSTK_License
for license details.

Created on February, 9, 2013

@author: Sourabh Bajaj
@contact: sourabhbajaj@gatech.edu
@summary: Python Validation Script
'''

# Printing what Python Version is installed : QSTK uses 2.7
from __future__ import division
import sys
import platform
import math
import os
import numpy
import matplotlib
import pandas
import scipy
import dateutil
import setuptools
import cvxopt
import datetime as dt
import QSTK
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkstudy.EventProfiler



#Function to Simulate a Portfolio


def fastSimulate(d_data, allocationSet):
    df_rets = d_data['close'].copy() #Copy the close data to df_rets
    na_rets = df_rets.values 
    normalize_returns = na_rets/na_rets[0,:] #Normalized Daily
    #print "Retorno Normalizado"
    #print normalize_returns
     
    daily_returns_allocated = normalize_returns *allocationSet #Daily Returns Allocated
    #print "Alocado"
    #print daily_returns_allocated
    
    sum_daily_ret_alloc = numpy.sum(daily_returns_allocated, axis=1, dtype=float)
    #print "Soma Alocado"
    #print sum_daily_ret_alloc
    
    next_day = sum_daily_ret_alloc[1:]
    actual_day = sum_daily_ret_alloc[:numpy.size(sum_daily_ret_alloc)-1]
    
    daily_returns = numpy.append(((next_day/actual_day) -1), 0)
    #print "Retorno Diario"
    #print daily_returns    
    
    
    sum_daily_ret_alloc_avg = numpy.mean(daily_returns, dtype=float)
    #print "Media dos retornos diarios"
    #print sum_daily_ret_alloc_avg
    
    sum_daily_ret_alloc_std = numpy.std(daily_returns, dtype=float)
    #print "STD dos retornos diarios"
    #print sum_daily_ret_alloc_std
    
    sharpe_ratio = math.sqrt(252) * sum_daily_ret_alloc_avg/sum_daily_ret_alloc_std
    #print "Sharpe Ratio"
    #print sharpe_ratio
    
    cum_return = sum_daily_ret_alloc[len(sum_daily_ret_alloc)-1]
    #print "Cumulative Return"
    #print cum_return    
    
    
    return sum_daily_ret_alloc_std, sum_daily_ret_alloc_avg, sharpe_ratio, cum_return
    
def getData(startdate, enddate, symbols):
    dt_start = startdate
    dt_end = enddate
    dt_timeofday = dt.timedelta(hours=16)
    
    
    
    # Get the working NYSE days   
    c_dataobj = da.DataAccess('Yahoo', cachestalltime=0) #Connect to Yahoo to get prices
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday) #Get the working days of the NYSE
    ls_all_syms = c_dataobj.get_all_symbols()#Get Data for all the symbols of NYSE
    ls_symbols = symbols #Get the symbols in interest
    ls_bad_syms = list(set(ls_symbols) - set(ls_all_syms)) #Remove non-existent (bad) symbols
    
    if len(ls_bad_syms) != 0: #If there is any wrong typed symbol in the ls_symbols
        print "Portfolio contains bad symbols : ", ls_bad_syms


    for s_sym in ls_bad_syms: #Remove the bad symbols
        i_index = ls_port_syms.index(s_sym)
        ls_port_syms.pop(i_index)
        lf_port_alloc.pop(i_index)
    
    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close'] #Create the keys we want to return

    ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys) #Get the Stock Data
    d_data = dict(zip(ls_keys, ldf_data)) #Create a dictionary to make easy to access the data
    
    return d_data
    
def simulate(startdate, enddate, symbols, allocationSet):
# Get the start, end and time of the day    
    dt_start = startdate
    dt_end = enddate
    dt_timeofday = dt.timedelta(hours=16)
    
    
    
    # Get the working NYSE days   
    c_dataobj = da.DataAccess('Yahoo', cachestalltime=0) #Connect to Yahoo to get prices
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday) #Get the working days of the NYSE
    ls_all_syms = c_dataobj.get_all_symbols()#Get Data for all the symbols of NYSE
    ls_symbols = symbols #Get the symbols in interest
    ls_bad_syms = list(set(ls_symbols) - set(ls_all_syms)) #Remove non-existent (bad) symbols
    
    if len(ls_bad_syms) != 0: #If there is any wrong typed symbol in the ls_symbols
        print "Portfolio contains bad symbols : ", ls_bad_syms


    for s_sym in ls_bad_syms: #Remove the bad symbols
        i_index = ls_port_syms.index(s_sym)
        ls_port_syms.pop(i_index)
        lf_port_alloc.pop(i_index)
    
    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close'] #Create the keys we want to return

    ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys) #Get the Stock Data
    d_data = dict(zip(ls_keys, ldf_data)) #Create a dictionary to make easy to access the data
    
    df_rets = d_data['close'].copy() #Copy the close data to df_rets
    na_rets = df_rets.values 
    normalize_returns = na_rets/na_rets[0,:] #Normalized Daily
    #print "Retorno Normalizado"
    #print normalize_returns
     
    daily_returns_allocated = normalize_returns *allocationSet #Daily Returns Allocated
    #print "Alocado"
    #print daily_returns_allocated
    
    sum_daily_ret_alloc = numpy.sum(daily_returns_allocated, axis=1, dtype=float)
    #print "Soma Alocado"
    #print sum_daily_ret_alloc
    
    next_day = sum_daily_ret_alloc[1:]
    actual_day = sum_daily_ret_alloc[:numpy.size(sum_daily_ret_alloc)-1]
    
    daily_returns = numpy.append(((next_day/actual_day) -1), 0)
    #print "Retorno Diario"
    #print daily_returns    
    
    
    sum_daily_ret_alloc_avg = numpy.mean(daily_returns, dtype=float)
    #print "Media dos retornos diarios"
    #print sum_daily_ret_alloc_avg
    
    sum_daily_ret_alloc_std = numpy.std(daily_returns, dtype=float)
    #print "STD dos retornos diarios"
    #print sum_daily_ret_alloc_std
    
    sharpe_ratio = math.sqrt(252) * sum_daily_ret_alloc_avg/sum_daily_ret_alloc_std
    #print "Sharpe Ratio"
    #print sharpe_ratio
    
    cum_return = sum_daily_ret_alloc[len(sum_daily_ret_alloc)-1]
    #print "Cumulative Return"
    #print cum_return    
    
    
    return sum_daily_ret_alloc_std, sum_daily_ret_alloc_avg, sharpe_ratio, cum_return

    
    
def optimizerDumb(dt_start, dt_end, ls_symbols):
    BigVol = None
    BigDR = None
    BigSharpe = None
    BigCR = None
    Alloc = None
    
    d_data = getData(dt_start, dt_end, ls_symbols)
    
    
    for a in range(0,11):
        for b in range(0,11):
            for c in range(0,11):
                for d in range(0,11):
                    if a+b+c+d == 10:
                        #vol, daily_ret, sharpe, cum_ret = simulate(dt_start, dt_end, ls_symbols, [a/10,b/10,c/10,d/10])
                        vol, daily_ret, sharpe, cum_ret = fastSimulate(d_data, [a/10,b/10,c/10,d/10])
                        if sharpe > BigSharpe:
                            BigSharpe = sharpe
                            BigVol = vol
                            BigDR = daily_ret
                            BigSharpe = sharpe
                            BigCR = cum_ret
                            Alloc = [a/10,b/10,c/10,d/10]
    return BigVol, BigDR, BigSharpe, BigCR, Alloc


# Checking that the data installed is correct.
# Start and End date of the charts
dt_start = dt.datetime(2011, 1, 1)
dt_end = dt.datetime(2011, 12, 31)
dt_timeofday = dt.timedelta(hours=16)

# Get a list of trading days between the start and the end.
ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
ls_symbols = ['BRCM', 'ADBE', 'AMD', 'ADI']

#vol, daily_ret, sharpe, cum_ret = simulate(dt_start, dt_end, ls_symbols, [0.4,0.4, 0.0,0.2])

BigVol, BigDR, BigSharpe, BigCR, Alloc = optimizerDumb(dt_start, dt_end, ls_symbols)


print "Start Date: " + str(dt_start)
print "End Date: " +str(dt_end)
print "Symbols: " + str(ls_symbols)
print "Optimal Allocation: " + str(Alloc)
print "Sharpe Ratio: " + str(BigSharpe)
print "Volatility: " + str(BigVol)
print "Average Daily Return: " + str(BigDR)
print "Cumulative Return: " + str(BigCR)
