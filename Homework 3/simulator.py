# -*- coding: utf-8 -*-
"""
Created on Sat Apr 06 11:51:07 2013

@author: Rafox
"""
import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import csv
import datetime
from datetime import date, timedelta
import sys

def getData(startdate, enddate, symbols):
    dt_start = startdate
    dt_end = enddate
    dt_timeofday = dt.timedelta(hours=16)
    
    
    
    # Get the working NYSE days   
    c_dataobj = da.DataAccess('Yahoo') #Connect to Yahoo to get prices
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday) #Get the working days of the NYSE
    ls_all_syms = c_dataobj.get_all_symbols()#Get Data for all the symbols of NYSE
    ls_symbols = symbols #Get the symbols in interest
    ls_bad_syms = list(set(ls_symbols) - set(ls_all_syms)) #Remove non-existent (bad) symbols
    
    if len(ls_bad_syms) != 0: #If there is any wrong typed symbol in the ls_symbols
        print "Portfolio contains bad symbols : ", ls_bad_syms


    for s_sym in ls_bad_syms: #Remove the bad symbols
        i_index = ls_port_syms.index(s_sym)
        ls_port_syms.pop(i_index)
        lf_port_alloc.pop(i_index)
    
    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close'] #Create the keys we want to return

    ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys) #Get the Stock Data
    #print ldf_data
    d_data = dict(zip(ls_keys, ldf_data)) #Create a dictionary to make easy to access the data
    #print d_data
    
    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method = 'ffill')
        d_data[s_key] = d_data[s_key].fillna(method = 'bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)
        
    return d_data

def getOrders(arquivo):
    with open(arquivo, "rU") as ifile:
        reader = csv.reader(ifile)
    
        listaData = []
        listaStock = []
        listaTipo = []
        listaQuant = []
    
        for item in reader:
            #print "%s-%s-%s" % (item[0],item[1],item[2])
            data = datetime.datetime(int(item[0]), int(item[1]), int(item[2]), 16, 0, 0)
            #print data
            stock = item[3]
            tipo = item[4]
            quant = item[5]
            listaStock.append(stock)
            listaData.append(data)
            listaTipo.append(tipo)
            listaQuant.append(quant)
            #print "%s - %s: %s - %s" % (str(data), stock, tipo, quant)
            # print listaStock
        
    
        dados = [listaStock, listaTipo, listaQuant]
        dados = np.array(zip(*dados))
        ordens = pd.DataFrame(dados, index=listaData, columns=["Stock", "Tipo", "Quant"])
    return ordens
           
def simulator(dados, symbols, orders, startMoney, startDate, endDate):
    dias = dados.index
    posicao = pd.DataFrame(index=dias, columns=symbols)
    posicao.fillna(value=0, axis={0,1}, inplace=True) #preenche de 0 o vetor posicao
    #print posicao[:5]
    caixa = pd.DataFrame(index=dias, columns=["valor"])
    #print posicao
    caixa["valor"] = startMoney    
    #verifica tipo de ordem, se eh buy ou sell e ativa a funcao necessaria
    for index, row in orders.iterrows():
        if row["Tipo"] == 'Buy':
            precoUn = dados.xs(index)[row["Stock"]]
            buy(caixa, posicao, row["Stock"], int(row["Quant"]), precoUn, index)
        if row["Tipo"] == 'Sell':
            precoUn = dados.xs(index)[row["Stock"]]
            sell(caixa, posicao, row["Stock"], int(row["Quant"]), precoUn, index)

    posicaoQuant = posicao*dados
    #print posicaoQuant[:30]
    caixaAcao = posicaoQuant.sum(axis=1)
    retorno = caixa + caixaAcao
        
    #print teste
    #print caixa[:30]
    #print orders
    return retorno
    

   
    
def writeCSV(retorno, arquivo):
        with open(arquivo, "wb") as ifile:
            writer = csv.writer(ifile)
        
            for index, row in retorno.iterrows():
                ano = index.year
                mes = str(index.month)
                dia = str(index.day)
                valor = str(row["valor"])
                teste = writer.writerow([ano, mes, dia, valor])
         
        return
            
def buy(caixa, posicao, symbol, quant, precoUn, date):
    
    for index, row in posicao.iterrows():
        if index >= date:
            posicao.xs(index, copy=False)[symbol] = row[symbol] + quant
            
    for index, row in caixa.iterrows():
        if index >= date:
            caixa.xs(index, copy=False)["valor"] = row["valor"] - precoUn*quant
            
    #print posicao
    #print caixa
    
    return
    
def sell(caixa, posicao, symbol, quant, precoUn, date):
    
    for index, row in posicao.iterrows():
        if index >= date:
            posicao.xs(index, copy=False)[symbol] = row[symbol] - quant
            
    for index, row in caixa.iterrows():
        if index >= date:
            caixa.xs(index, copy=False)["valor"] = row["valor"] + precoUn*quant
            
    #print posicao
    #print caixa
    
    return

if __name__ == '__main__':
    startMoney = int(sys.argv[1])
    ordersFile = sys.argv[2]
    returnFile = sys.argv[3]
    orders = getOrders(ordersFile)
    print "Arquivo de ordens:"
    #print orders
    symbols = set(orders["Stock"])
    print "Simbolos contidos no arquivo de ordens:"
    print symbols
    startDate = orders.index[0]
    #Gambi para virar datetime.datetime
    #startDate = datetime.datetime(startDate.year, startDate.month, startDate.day)
    print "Data inicial:"
    print startDate
    endDate = orders.tail(1).index[0]
    #Gambi para virar datetime.datetime
    #endDate = datetime.datetime(endDate.year, endDate.month, endDate.day)
    print "Data final:"
    print endDate
    
    d_data = getData(startDate, endDate, symbols)
    d_rets = d_data["close"].copy()
    #print d_rets.xs(startDate)
    #print dadosIndex
    retorno = simulator(d_rets, symbols, orders, startMoney, startDate, endDate)
    #print df_rets.values
    
    writeCSV(retorno, returnFile)

    