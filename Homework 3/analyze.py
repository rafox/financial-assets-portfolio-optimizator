# -*- coding: utf-8 -*-
"""
Created on Sun Apr 07 12:19:13 2013

@author: Rafox
"""

import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep
import csv
import datetime
from datetime import date, timedelta
import sys
import matplotlib.pyplot as plt

def getData(startdate, enddate, symbols):
    dt_start = startdate
    dt_end = enddate
    dt_timeofday = dt.timedelta(hours=16)
 
       
    # Get the working NYSE days   
    c_dataobj = da.DataAccess('Yahoo', cachestalltime=0) #Connect to Yahoo to get prices
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday) #Get the working days of the NYSE
    ls_all_syms = c_dataobj.get_all_symbols()#Get Data for all the symbols of NYSE
    ls_symbols = symbols #Get the symbols in interest
    ls_bad_syms = list(set(ls_symbols) - set(ls_all_syms)) #Remove non-existent (bad) symbols
    
    if len(ls_bad_syms) != 0: #If there is any wrong typed symbol in the ls_symbols
        print "Portfolio contains bad symbols : ", ls_bad_syms


    for s_sym in ls_bad_syms: #Remove the bad symbols
        i_index = ls_port_syms.index(s_sym)
        ls_port_syms.pop(i_index)
        lf_port_alloc.pop(i_index)
    
    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close'] #Create the keys we want to return

    ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys) #Get the Stock Data
    #print ldf_data
    d_data = dict(zip(ls_keys, ldf_data)) #Create a dictionary to make easy to access the data
    #print d_data
    
    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method = 'ffill')
        d_data[s_key] = d_data[s_key].fillna(method = 'bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)
        
    return d_data

def getReturnFromFile(arquivo):
    with open(arquivo, "rb") as ifile:
        reader = csv.reader(ifile)
        listaData = []
        listaValor = []
    
        for item in reader:
            data = datetime.datetime(int(item[0]), int(item[1]), int(item[2]), 16, 0, 0)
            valor = float(item[3])
        
            listaData.append(data)
            listaValor.append(valor)
        
    
        retornos = pd.DataFrame(listaValor, index=listaData, columns=["valor"])
    return retornos

def acessPerformance(sum_daily_ret_alloc):
    #print sum_daily_ret_alloc
    next_day = sum_daily_ret_alloc[1:]
    actual_day = sum_daily_ret_alloc[:np.size(sum_daily_ret_alloc)-1]
    daily_returns = np.append(((next_day/actual_day) -1), 0)
    sum_daily_ret_alloc_avg = np.mean(daily_returns, dtype=float)
    sum_daily_ret_alloc_std = np.std(daily_returns, dtype=float)
    sharpe_ratio = math.sqrt(252) * sum_daily_ret_alloc_avg/sum_daily_ret_alloc_std
    cum_return = sum_daily_ret_alloc[len(sum_daily_ret_alloc)-1]
    total_return = cum_return/sum_daily_ret_alloc[0]
    
    return sum_daily_ret_alloc_std, sum_daily_ret_alloc_avg, sharpe_ratio, cum_return, total_return

if __name__ == '__main__':
    arquivoLeitura = sys.argv[1]
    indiceComp = sys.argv[2]
    
    retorno = getReturnFromFile(arquivoLeitura)
    symbols = [indiceComp]
    startDate = retorno.index[0]
    endDate = retorno.tail(1).index[0]
    startMoney = retorno["valor"][0]
    indice_data = getData(startDate, endDate, symbols)
    close_indice = indice_data["close"]
    
    i_sum_daily_ret_alloc_std, i_sum_daily_ret_alloc_avg, i_sharpe_ratio, i_cum_return, i_total_return = acessPerformance(close_indice.as_matrix(columns=[indiceComp]))
    #print retorno[:30]
    sum_daily_ret_alloc_std, sum_daily_ret_alloc_avg, sharpe_ratio, cum_return, total_return = acessPerformance(retorno.as_matrix(columns=["valor"]))
    
    print "The final value of the portfolio using the sample file is -- " + str(retorno.tail(1).index[0]) + " - " + str(retorno.xs(endDate)["valor"])    
    
    print "Details of the performance portfolio:\n"
    
    print "Data Range: " + str(startDate) + " to " + str(endDate) + "\n" 

    print "Sharpe Ratio of Fund: " + str(sharpe_ratio)
    print "Sharpe Ratio of " + str(indiceComp) + ": " + str(i_sharpe_ratio) + "\n"

    print "Total Return of Fund: " + str(total_return[0])
    print "Total Return of " + str(indiceComp) + ": " + str(i_total_return[0]) + "\n"
    
    
    print "Standard Deviation of Fund: " + str(sum_daily_ret_alloc_std)
    print "Standard Deviation of " + str(indiceComp) + ": " + str(i_sum_daily_ret_alloc_std) + "\n"
    
    
    print "Average Daily Return of Fund: " + str(sum_daily_ret_alloc_avg)
    print "Average Daily Return of " + str(indiceComp) + ": " + str(i_sum_daily_ret_alloc_avg) + "\n"

    ret_fundo_valor = retorno["valor"].values
    share_indice = startMoney/close_indice[indiceComp].values[0]
    ret_indice_valor = close_indice[indiceComp].values*share_indice
    ret_datas = retorno.index.values
    plt.clf()
    plt.plot(ret_datas, ret_fundo_valor)
    plt.plot(ret_datas, ret_indice_valor)
    plt.legend(["Fund", indiceComp])
    plt.ylabel('Fund Value')
    plt.xlabel('Date')
    plt.savefig('fundperformance.pdf', format='pdf')
    
    
    
    